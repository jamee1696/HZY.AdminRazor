using System;

namespace HZY.Redis.Service
{
    using StackExchange.Redis;

    /// <summary>
    /// Redis 服务类
    /// </summary>
    public class RedisService : IRedisService
    {
        /// <summary>
        /// get database
        /// </summary>
        public IDatabase Database { get; }

        private readonly ConnectionMultiplexer connectionMultiplexer;

        public RedisService(string connectionString)
        {
            this.connectionMultiplexer = ConnectionMultiplexer.Connect(connectionString);
            this.Database = connectionMultiplexer.GetDatabase();
        }

        public void Dispose()
        {
            if (this.connectionMultiplexer != null)
            {
                this.connectionMultiplexer.Close();
                this.connectionMultiplexer.Dispose();
            }
        }



    }
}