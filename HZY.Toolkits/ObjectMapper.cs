﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace HZY.Toolkits
{
    using AutoMapper;

    /// <summary>
    /// 对象映射类
    /// </summary>
    public static class ObjectMapper
    {

        /// <summary>
        /// copy object
        /// </summary>
        /// <typeparam name="OldT1">旧对象类型</typeparam>
        /// <typeparam name="NewT2">新复制对象类型</typeparam>
        /// <param name="Source"></param>
        /// <returns></returns>
        public static NewT2 MapTo<OldT1, NewT2>(this OldT1 Source)
            where OldT1 : class
            where NewT2 : class

        {
            if (Source == null) return default;

            var config = new MapperConfiguration(cfg => cfg.CreateMap<OldT1, NewT2>());
            var mapper = config.CreateMapper();

            return mapper.Map<NewT2>(Source);
        }

        /// <summary>
        /// copy list
        /// </summary>
        /// <typeparam name="OldT1">旧对象类型</typeparam>
        /// <typeparam name="NewT2">新复制对象类型</typeparam>
        /// <param name="Source"></param>
        /// <returns></returns>
        public static List<NewT2> MapToList<OldT1, NewT2>(this IEnumerable<OldT1> Source)
            where OldT1 : class
            where NewT2 : class

        {
            if (Source == null) return default;

            var config = new MapperConfiguration(cfg => cfg.CreateMap<OldT1, NewT2>());
            var mapper = config.CreateMapper();

            return mapper.Map<List<NewT2>>(Source);
        }


    }
}
